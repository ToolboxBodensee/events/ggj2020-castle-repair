# Castle Repair

This project can be found at https://gitlab.com/ToolboxBodensee/events/ggj2020-castle-repair

## About the game

The theme of the Global Game Jam 2020 is 'repair'.
The game is made with Unity (2019.1.14f1) using free assets from the Unity asset store and own assets.
All own assets and code was created while the jam.

We created a game which might remind on tower defense or castle defense games.
Waves of enemys are trying to destroy towers, walls and finally getting to the gates.
Your job as player is to repair the towers and walls so they do not get through.

We may integrate a web playable improved version on the toolbox-bodensee.de website the next days.

## Gameplay / Controls

You score points for dead attackers. Those points can get used to repair towers and walls by clicking on them.
Of course the object clicked should be damaged and you should have enough points to repair it.
Repair costs are not displayed yet - sorry.

## About our team

We worked in a team of three people who never worked together on a project before this jam.
We however did use Unity before but not much.

## Building the game

Nothing special is needed to build the game beside a Unity (version 2019.1.14f1) installation.
Everything goes the normal Unity way of building releases.
