﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EasyLevel : MonoBehaviour
{
    public Transform[] Gates;
    public List<Wave> waves = new List<Wave>();
    public bool finalLevel;
    public GameObject won;

    private bool hasWon = false;
    
    void Start()
    {
        SpawnWave();
    }
    
    void Update()
    {
        if (GameObject.FindGameObjectsWithTag("Enemy").Length == 0 && !hasWon)
        {
            enabled = false;
            won.SetActive(true);
            hasWon = true;
            Time.timeScale = 0;
        }

        if (hasWon && Input.anyKey && !finalLevel)
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        else if (hasWon && Input.anyKey)
            SceneManager.LoadScene(0);
    }

    private void SpawnWave()
    {
        if (waves.Count == 0)
        {
            return;
        }
        
        var wave = waves[0];
        waves.Remove(wave);

        float x = 0;
        
        foreach (var spawn in wave.toSpawn)
        {
            float y = -30;
            
            for (var i = 0; i < spawn.amount; i++)
            {
                Instantiate(spawn.toSpawn, transform.position + new Vector3(x, 0, y), Quaternion.identity)
                    .GetComponent<EnemyBase>().Gates = Gates;
                y += 5f;
            }

            x -= 5f;
        }
        
        Invoke(nameof(SpawnWave), wave.timeToNext);
    }
}

[Serializable]
public class Wave
{
    public List<Spawn> toSpawn = new List<Spawn>();
    public int timeToNext = 5;
}

[Serializable]
public class Spawn
{
    public GameObject toSpawn;
    public int amount;
}
