using UnityEngine;
using System.Collections;

public class Point : MonoBehaviour {
	public float point;
	private float getHitEffect;
	private float targY;
	private Vector3 pointPosition;

	public GUISkin pointSkin;
	public GUISkin pointSkinShadow;

	void Start() {
		pointPosition = transform.position + new Vector3(Random.Range(-1,1),0,Random.Range(-1,1));
		targY = Screen.height /2;
	}

	void OnGUI() {
		Vector3 screenPos2 = Camera.main.GetComponent<Camera>().WorldToScreenPoint (pointPosition);
		getHitEffect += Time.deltaTime*30;
		GUI.color = new Color (1.0f,1.0f,1.0f,1.0f - (getHitEffect - 50) / 7);
		GUI.skin = pointSkinShadow;
		string pointText;
		if (point >= 0) {
			pointText = "+" + point.ToString();
		} else {
			pointText = point.ToString();
		}
		GUI.Label (new Rect (screenPos2.x+8 , targY-2, 80, 70), pointText);
		GUI.skin = pointSkin;
		GUI.Label (new Rect (screenPos2.x+10 , targY, 120, 120), pointText);
	}

	void Update() {
		targY -= Time.deltaTime*200;
	}
}
