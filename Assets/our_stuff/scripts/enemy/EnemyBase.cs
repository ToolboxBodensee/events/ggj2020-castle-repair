﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavigationBase))]
public class EnemyBase : Damageable
{
    public Transform[] Gates;
    public float attackTurretRelevanceDistance = 10;
    public float attackRadius = 1.5f;
    public float attackDamage = 7.5f;
    public float attackCooldown = 1.5f;
	public float pointFrom = 10;
	public float pointTo = 15;
    public GameObject point;

    private bool isTrackingTurret;
    private TurretBase trackedTurret;
    private List<TurretBase> turrets = new List<TurretBase>();
    private float currentAttackCooldown = 0;
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        foreach (var turret in GameObject.FindGameObjectsWithTag("Turret"))
        {
            if (turret.GetComponent<TurretBase>() != null)
                turrets.Add(turret.GetComponent<TurretBase>());
            else
                Debug.LogWarning($"Found Object \"{turret.name}\" with tag \"Turret\" " +
                                 "without a TurretBase component. This is not allowed");
        }

        CheckTurretRelevance();
        isEnemy = true;

        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (transform.position.y < -1000)
        {
            Destroy(gameObject);
            return;
        }
        
        if (currentAttackCooldown != 0f)
        {
            currentAttackCooldown -= Time.deltaTime;

            if (currentAttackCooldown < 0)
                currentAttackCooldown = 0;
        }

        if (isTrackingTurret)
            TurretTrackUpdate();
        else
            GateTrackUpdate();
        
        if (anim)
            anim.SetBool("run", !GetComponent<NavMeshAgent>().isStopped);
    }

    private void TurretTrackUpdate()
    {
        if (trackedTurret == null)
        {
            if (GetComponent<NavMeshAgent>().isStopped)
                GetComponent<NavMeshAgent>().isStopped = false;
            CheckTurretRelevance();
            return;
        }

        GetComponent<NavMeshAgent>().isStopped = Vector3.Distance(trackedTurret.transform.position, transform.position) <= attackRadius;

        if (Vector3.Distance(trackedTurret.transform.position, transform.position) <= attackRadius
            && currentAttackCooldown <= 0)
        {
            currentAttackCooldown = attackCooldown;
            trackedTurret.TakeDamage(attackDamage);
                        
            if (anim)
                anim.SetTrigger("Attack");
            
            return;
        }

        if (Vector3.Distance(trackedTurret.transform.position, transform.position) <= attackRadius)
            return;

        CheckTurretRelevance();
    }

    private void GateTrackUpdate()
    {
        var tracked = GetComponent<NavigationBase>().target;

        GetComponent<NavMeshAgent>().isStopped = Vector3.Distance(tracked.position, transform.position) <= attackRadius;

        if (Vector3.Distance(tracked.position, transform.position) <= attackRadius
            && currentAttackCooldown <= 0)
        {
            currentAttackCooldown = attackCooldown;
            tracked.GetComponent<Damageable>().TakeDamage(attackDamage);
            
            if (anim)
                anim.SetTrigger("Attack");
            
            return;
        }

        if (Vector3.Distance(tracked.position, transform.position) <= attackRadius)
            return;

        CheckTurretRelevance();
    }

    /// <summary>
    /// Checks the relevance of attacking a turret.
    /// Will automatically track a turret if relevant.
    /// </summary>
    private void CheckTurretRelevance()
    {
        var target = GetNearest(Gates, out float dist);

        if (dist <= 15)
        {
            isTrackingTurret = false;
            trackedTurret = null;
            GetComponent<NavigationBase>().target = target;
            return;
        }

        var turret = GetNearest(turrets.ToArray(), out dist);

        if (dist <= attackTurretRelevanceDistance)
        {
            //Debug.Log("Tracking Turret!");
            isTrackingTurret = true;
            trackedTurret = turret.GetComponent<TurretBase>();
            GetComponent<NavigationBase>().target = turret;
            return;
        }

        isTrackingTurret = false;
        trackedTurret = null;

        target = GetNearest(Gates, out dist);

        GetComponent<NavigationBase>().target = target;
    }

    #region Utility

    private Transform GetNearest(Transform[] objects, out float smallestDistance)
    {
        smallestDistance = 9999;
        Transform nearest = null;

        foreach (var t in objects)
        {
            if (Vector3.Distance(t.position, transform.position) > smallestDistance)
                continue;

            nearest = t;
            smallestDistance = Vector3.Distance(t.position, transform.position);
        }

        return nearest;
    }

    private Transform GetNearest(MonoBehaviour[] objects, out float smallestDistance)
    {
        smallestDistance = 9999;
        Transform nearest = null;

        foreach (var t in objects)
        {
            if (Vector3.Distance(t.transform.position, transform.position) > smallestDistance)
                continue;

            nearest = t.transform;
            smallestDistance = Vector3.Distance(t.transform.position, transform.position);
        }

        return nearest;
    }

    #endregion

    protected override void OnDeath()
    {
        int points = (int)Mathf.Round(Random.Range(pointFrom, pointTo));
        GameObject go = GameObject.Find("ScoreManager");
        go.GetComponent<ScoreManager>().ChangeScore(points);
        GameObject pointgo = Instantiate(point, transform.position, transform.rotation);
        pointgo.GetComponent<Point>().point = points;
        pointgo.GetComponent<Point>().enabled = true;
        Destroy(pointgo, 1.5f);
        
        if (anim)
            anim.SetBool("dead", true);

        enabled = false;
        Destroy(gameObject, 2);
    }

    public void StopTrackingTurret(TurretBase tb)
    {
        turrets.Remove(tb);
    }
}
