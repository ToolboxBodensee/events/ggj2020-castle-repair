﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class NavigationBase : MonoBehaviour
{
    public Transform target;
    
    void Update()
    {
        if (target)
            GetComponent<NavMeshAgent>().SetDestination(target.position);
        else if (!GetComponent<NavMeshAgent>().isStopped)
            GetComponent<NavMeshAgent>().isStopped = true;
    }
}
