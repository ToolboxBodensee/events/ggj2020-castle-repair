﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public float spawnPause = 15;
    public float initialSpawnPause = 3;
    public float spawnCountFactor = 0.5f;
    //public float spawnPauseModifier = 0;
    public GameObject[] EnemyTypes;
    public Transform[] TargetPositions;
    public AudioClip spawnClip;

    private float spawnPauseTimer;
    private int spawnRound = 0;

    // Start is called before the first frame update
    void Start()
    {
        spawnPauseTimer = initialSpawnPause;
    }

    // Update is called once per frame
    void Update()
    {
        spawnPauseTimer -= Time.deltaTime;
        if ( spawnPauseTimer < 0 )
        {
            spawnPauseTimer = spawnPause;
            Spawn();
        }
    }

    void Spawn()
    {
        Debug.Log("spawning phase");
        spawnRound++;
        int enemyRandSpawnCount = (int) Mathf.Round(Random.Range(0, spawnCountFactor * spawnRound));
        for (int i = 0; i < enemyRandSpawnCount; i++)
        {
            int whichOne = (int) Mathf.Round(Random.Range(0, EnemyTypes.Length));
            GameObject enemySpawned = Instantiate(EnemyTypes[whichOne], transform.position, transform.rotation);
            enemySpawned.GetComponent<EnemyBase>().Gates = TargetPositions;
        }

        if (spawnClip)
            AudioSource.PlayClipAtPoint(spawnClip, transform.position);
    }
}
