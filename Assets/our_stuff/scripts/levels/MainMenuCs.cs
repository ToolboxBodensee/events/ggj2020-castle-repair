﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuCs : MonoBehaviour
{
    public float timeScale = .25f;
    
    void Start()
    {
        Time.timeScale = timeScale;
    }
    
    public void Exit()
    {
        Application.Quit();
    }
    
    public void PlayEndless()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(1);
    }
    
    public void Play()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(2);
    }
}
