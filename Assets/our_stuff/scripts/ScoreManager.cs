﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public Text scoreText;
    
    public int score = 0;

    private void Start()
    {
        ChangeScore(0);
    }

    // change score
    public bool ChangeScore(int scoreChange)
    {
        if (score + scoreChange < 0 )
        {
            return false;
        }
        
        score += scoreChange;
        scoreText.text = score.ToString();
        return true;
    }
}
