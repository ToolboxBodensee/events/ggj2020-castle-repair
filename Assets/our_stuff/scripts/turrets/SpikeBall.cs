﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeBall : MonoBehaviour
{
    public float damage = 30;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Spike Collision");
        
        if (collision.gameObject.GetComponent<Damageable>() && collision.gameObject.GetComponent<Damageable>().isEnemy)
            collision.gameObject.GetComponent<Damageable>().TakeDamage(damage);
    }
}
