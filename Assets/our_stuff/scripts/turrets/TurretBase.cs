﻿using System;
using UnityEngine;

public class TurretBase : Damageable
{
    public GameObject Cannonball;
    public Transform turret;
    public Transform firepoint;
    public float Range = 15;
    public bool EnergyTurret;
    public float ReloadTime = 3;
    public float EnergyTurretAttackDamage = 10;
    public Effect effect;
    public Effect deathEffect;
    public AudioClip shoot;

    protected float PreparationTime = 0;

    protected override void OnDeath()
    {
        Destroy(gameObject);
        
        foreach (var e in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            if (e.GetComponent<EnemyBase>())
                e.GetComponent<EnemyBase>().StopTrackingTurret(this);
        }
        
        if (deathEffect)
            deathEffect.PerformEffect(transform);
    }
    
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (PreparationTime > 0) PreparationTime -= Time.deltaTime;
//        Debug.Log("Time: " + PreparationTime);
        SelectEnemy();
    }

    protected virtual void Fire(GameObject target)
    {
        turret.LookAt(target.transform);
        Physics.Raycast(firepoint.position, firepoint.forward, out var hit);
        
        if (PreparationTime <= 0 && hit.transform == target.transform)
        {
            PreparationTime = ReloadTime;
            
            if (!EnergyTurret)
            {
                var c = Instantiate(Cannonball, firepoint.position, firepoint.rotation);
                c.GetComponent<TrackEnemy>().enemy = target.transform;
                Destroy(c, 5f);
                
                if (effect)
                    effect.PerformEffect(firepoint);
            }
            else
            {
                target.GetComponent<Damageable>().TakeDamage(EnergyTurretAttackDamage);
                
                if (effect)
                    effect.PerformEffect(target.transform);
            }
            
            if (shoot)
                AudioSource.PlayClipAtPoint(shoot, firepoint.position);
        }
    }

    void SelectEnemy()
    {
        var enemies = GameObject.FindGameObjectsWithTag("Enemy");

        GameObject targetenemy = null;
        float mindist = 99999999;
        //Debug.Log("MinDist: " + result);
        foreach (var enemy in enemies)
        {
            float dist = Vector3.Distance(transform.position, enemy.transform.position);
            if (Range < dist) continue;
            if (targetenemy == null || (targetenemy != null && mindist > dist))
            {
                targetenemy = enemy;
                mindist = dist;
            }
        }
        if (targetenemy != null)
        {
            //Debug.Log("Target gefunden!" + mindist);
            Fire(targetenemy);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(transform.position, Range);
    }
}

