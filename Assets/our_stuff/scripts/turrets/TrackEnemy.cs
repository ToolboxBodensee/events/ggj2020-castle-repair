﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackEnemy : MonoBehaviour
{
    public Transform enemy;
    public float speed = 30;
    public float damage = 25f;

    void Update()
    {
        if (enemy == null)
        {
            Destroy(gameObject);
            return;
        }

        if (Vector3.Distance(transform.position, enemy.position) < speed * Time.deltaTime)
            OnHit();
        else
        {
            transform.LookAt(enemy);
            transform.Translate(new Vector3(0, 0, speed * Time.deltaTime), Space.Self);
        }
    }

    private void OnHit()
    {
        Destroy(gameObject);
        enemy.GetComponent<Damageable>().TakeDamage(damage);
    }
}

