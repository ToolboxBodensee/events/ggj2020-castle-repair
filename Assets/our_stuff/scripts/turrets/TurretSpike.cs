﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretSpike : TurretBase
{
    public GameObject Spikeball;


    protected override void OnDeath()
    {
        Destroy(gameObject);
        
        foreach (var e in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            if (e.GetComponent<EnemyBase>())
                e.GetComponent<EnemyBase>().StopTrackingTurret(this);
        }
        
        if (deathEffect)
            deathEffect.PerformEffect(transform);
    }
    
    // Start is called before the first frame update
    void Start()
    {

    }


    protected override void Fire(GameObject target)
    {
        turret.LookAt(target.transform);
        Physics.Raycast(firepoint.position, firepoint.forward, out var hit);
        if (PreparationTime <= 0)
        {
            PreparationTime = ReloadTime;
            if (!EnergyTurret)
            {
                var c = Instantiate(Spikeball, firepoint.position, firepoint.rotation);
                c.GetComponent<TrackEnemy>().enemy = target.transform;
                Destroy(c, 5f);
            }
            else
            {
                target.GetComponent<Damageable>().TakeDamage(EnergyTurretAttackDamage);
            }
            
            if (effect)
                effect.PerformEffect(target.transform);
        }
    }
}