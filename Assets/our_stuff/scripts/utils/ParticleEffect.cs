﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleEffect : Effect
{
    public GameObject effectSystem;

    public override void PerformEffect(Transform target)
    {
        var g = Instantiate(effectSystem, target.position, target.rotation);

        Destroy(g, 5f);
    }
}
