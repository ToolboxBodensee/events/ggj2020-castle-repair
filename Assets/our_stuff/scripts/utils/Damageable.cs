﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

public abstract class Damageable : MonoBehaviour
{
    public float health { get; protected set; } = 0;
    public float maxHealth = 20;
    public float repairCostFactor = 1;
    public bool isEnemy;
    public AudioClip hit;
    public AudioClip death;
    
    private SimpleHealthBar healthBar;

    private void Awake()
    {
        healthBar = GetComponentInChildren<SimpleHealthBar>();

        health = maxHealth;
        UpdateVisuals();
    }

    public void TakeDamage(float amount)
    {
        if (amount <= 0f)
        {
            Debug.LogWarning($"TakeDamage was called with an amount of 0 or less " +
                             $"damage. This is not allowed. StackTrace: {Environment.StackTrace}");
            return;
        }

        health -= amount;

        if (health <= 0)
        {
            OnDeath();
            if (death)
                AudioSource.PlayClipAtPoint(death, transform.position);
        }
        else if (hit)
            AudioSource.PlayClipAtPoint(hit, transform.position);
        
        UpdateVisuals();
    }

    public void Heal(float amount)
    {
        if (amount <= 0f)
        {
            Debug.LogWarning($"Heal was called with an amount of 0 or less " +
                             $"damage. This is not allowed. StackTrace: {Environment.StackTrace}");
            return;
        }

        health += amount;

        if (health > maxHealth)
            health = maxHealth;

        UpdateVisuals();
    }

    private void UpdateVisuals()
    {
        if (!healthBar)
            return;
        
        if (health == maxHealth)
            healthBar.transform.parent.gameObject.SetActive(false);
        else
        {
            healthBar.transform.parent.gameObject.SetActive(true);
            healthBar.UpdateBar(health, maxHealth);
            healthBar.UpdateColor(health * 5 < maxHealth ? Color.red : Color.green);
        }
    }

    protected abstract void OnDeath();

    void OnMouseDown()
    {
        if (isEnemy)
            return;
        
        GameObject go = GameObject.Find("ScoreManager");
        float missingHealth = maxHealth - health;
        int repairCosts = (int)(missingHealth * repairCostFactor);
        bool repairOk = go.GetComponent<ScoreManager>().ChangeScore(-repairCosts);
        if(repairOk)
        {
            Debug.Log("healing");
            Heal(missingHealth);
        } else {
            Debug.Log("TODO: not enough resources to repair");
        }
    }
}
