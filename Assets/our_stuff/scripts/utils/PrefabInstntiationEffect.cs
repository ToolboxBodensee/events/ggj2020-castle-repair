﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabInstntiationEffect : Effect
{
    public GameObject prefab;
    
    public override void PerformEffect(Transform target)
    {
        if (prefab)
            Instantiate(prefab, target.position, target.rotation);
    }
}
