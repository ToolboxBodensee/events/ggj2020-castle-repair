﻿using System.Collections;
using System.Collections.Generic;
using DigitalRuby.LightningBolt;
using UnityEngine;

public class LightningEffect : Effect
{
    public LightningBoltScript lightning;
    
    public override void PerformEffect(Transform target)
    {
        lightning.gameObject.SetActive(true);
        lightning.EndObject = target.gameObject;
        Invoke(nameof(SetEffectInvisible), 0.25f);
    }

    public void SetEffectInvisible()
    {
        lightning.gameObject.SetActive(false);
    }
}
