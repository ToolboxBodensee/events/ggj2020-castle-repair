﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Gate : Damageable
{
    public bool lost = false;
    public GameObject lostScreen;
    
    protected override void OnDeath()
    {
        Time.timeScale = 0;
        lost = true;
        lostScreen.SetActive(true);
    }

    private void Update()
    {
        if (lost && Input.anyKeyDown)
            SceneManager.LoadScene(0);
    }
}
